import React from 'react';
import Button from '@material-ui/core/Button';
import { withFirebase } from '../Firebase';
import { withRouter } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';

const SignOutButton = ({ firebase, history }) => (
  <Button color="inherit" onClick={ ()=> {
    firebase.doSignOut().then(()=> {
      history.push(ROUTES.SIGN_IN);
    })
  }}>
    Sign Out
  </Button>
);

export default withRouter(withFirebase(SignOutButton)); 