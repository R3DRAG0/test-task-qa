import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
      width: 25,
      height: 25,
      borderRadius: '100%',
      display: 'inline-block',
      marginRight: 4,
    },
  }));

const Point = ({color}) => {
    const classes = useStyles();

    return (
        <div className={classes.root} style={{backgroundColor: color}}/>
    );
}

export default Point;