import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import CardActions from '@material-ui/core/CardActions';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
    card: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    cardMedia: {
      paddingTop: '56.25%', // 16:9
    },
    cardContent: {
      flexGrow: 1,
    },
  }));

const Product = ({product, compare}) => {
    const classes = useStyles();

    return (
        <Grid item key={product.id} xs={12} sm={6} md={3}>
            <Card className={classes.card}>
                <CardMedia
                    className={classes.cardMedia}
                    image={product.image}
                    title={product.name}
                    alt={product.name}
                />
                <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                        {product.name}
                    </Typography>
                    <Typography>
                        {product.price}
                    </Typography>
                    <p>{product.description}</p>
                </CardContent>
                <CardActions>
                    <Button color="primary" onClick={() => compare(product)}>
                        {product.compare ? "Remove" : "Compare"}
                    </Button>
                  </CardActions>
            </Card>
        </Grid>
    );
}

export default Product